﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Bola : MonoBehaviour {
    private Rigidbody rb;
    public Text punts;
    public int point;
    public AudioClip impact;
    public AudioSource audio;
    public GameObject x3;
    private adalt multi;
    private float multiplica;
    private bool restaseg = false;
    private int suma = 10;
    // Use this for initialization
    void Start () {
        multiplica = 2;
        rb = GetComponent<Rigidbody>();
        point = 0;
        multi = x3.GetComponent<adalt>();
        audio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        //rb.velocity = new Vector3(0, 0,-2*Time.deltaTime);
        Physics.gravity = new Vector3(0, -10, -25);
        if (multi.puja==true) { restaseg = true;Debug.Log("hola"); }
        if(restaseg && multiplica>0)
        {
            multiplica -= Time.deltaTime;
            suma = 30;
        }
        else
        {
            restaseg = false;
            multiplica = 2;
            suma = 10;
        }
    
    }
    void OnCollisionEnter(Collision colis)
    {

        if (colis.gameObject.tag == "bumper")
        {
            point += suma;
            SetCountText();
            audio.Play();
        }
      
    }
    void SetCountText()
    {
        punts.text = "Score: " + point.ToString();
    }
}
