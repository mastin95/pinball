﻿using UnityEngine;
using System.Collections;

public class bumper : MonoBehaviour {
    public GameObject bola;
    private Light llum;
    public Rigidbody ball;
    private bool wait =false;
    private float waitlight = 0;
    private float waitlight2 = 0;
    public float thrust;
    public float right;
	// Use this for initialization
	void Start () {
        llum = GetComponentInChildren<Light>();
        llum.enabled = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (wait && waitlight > 0)
        {
            waitlight -= Time.deltaTime;
        }
        else
        {
            wait = false;
            llum.enabled = false;
        }
    }
    void OnCollisionEnter(Collision colis)
    {
        if (colis.gameObject.tag == "ball")
        {
            ball.AddForce(new Vector3(ball.transform.forward.x * right, 0, ball.transform.forward.z * thrust), ForceMode.Impulse);
            llum.enabled = true;
            waitlight = 0.15F;
            wait = true;
        }


    }
    
}
