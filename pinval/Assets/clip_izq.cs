﻿using UnityEngine;
using System.Collections;

public class clip_izq : MonoBehaviour {

    // Use this for initialization

    //Parameters
    float restPosition = 0F;
    public float pressedPosition = 45F;
    public float flipperStrength = 100F;
    float flipperDamper = 1F;
    JointLimits limites;
    System.String inputButtonName = "LeftPaddle";



    // Use this for initialization

    void Start()
    {
        GetComponent<HingeJoint>().useSpring = true;
    }

    // Update is called once per frame
    void Update()
    {
        JointSpring spring = new JointSpring();

        spring.spring = flipperStrength;
        spring.damper = flipperDamper;

        if (Input.GetKey(KeyCode.A))
            spring.targetPosition = pressedPosition;
        else
            spring.targetPosition = restPosition;

        GetComponent<HingeJoint>().spring = spring;
        GetComponent<HingeJoint>().useLimits = true;
        limites.min = restPosition;
        limites.max = pressedPosition;
        GetComponent<HingeJoint>().limits = limites;

    }
}
