﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class insertc : MonoBehaviour {

    public GameObject bola;
    public GameObject tirabol;
    public Transform respbol;
    public Text score;
    public Text life;
    public Light llum;
    private float zero = 0;
    private float vida = 3;
	// Use this for initialization
	void Start ()
    {
        llum.enabled = false;
       bola.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnMouseDown()
    {
        tirabol.SetActive(true);
        SetCountText();
        llum.enabled = true;
        bola.SetActive(true);
    }
    void SetCountText()
    {
       score.text = "Score: " + zero.ToString();
        life.text = "Life: " + vida.ToString();
    }
}
