﻿using UnityEngine;
using System.Collections;

public class spring : MonoBehaviour {
    public Rigidbody ball;
    public GameObject bola;
    private BoxCollider espring;
    private bool ready = false;
    private bool fire = false;
    public float thrust;
    private float fors=0;
    // Use this for initialization
    void Start () {
        espring = GetComponent<BoxCollider>();
        bola = GameObject.Find("Boleta");
        ball = bola.GetComponent<Rigidbody>();
	}
   void OnCollisionEnter(Collision colis)
    {
        if (colis.gameObject.tag == "ball")
        {
            ready = true;
        }
    }

    // Update is called once per frame
    void Update ()
    {

        if (Input.GetKey(KeyCode.Space))
        {
            fors += thrust;
            if (fors >= 25) fors = 25;
            Debug.Log(fors);
            fire = true;
        }
        if (ready && fire && Input.GetKeyUp(KeyCode.Space))
        {
            ball.AddForce(new Vector3(0, 0, ball.transform.forward.z * fors), ForceMode.Impulse);
            ready = false;
            fors = 0;
        }
        // Debug.Log(espring.transform.position.z - ball.transform.position.z);


    }
 

}
